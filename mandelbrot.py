
# coding: utf-8

# In[14]:

'''
MANDELBROT.PY

This program computes the Mandelbrot 
fractal with the Mandelbrot iteration 
on a grid whose x values range from -2
to 1 and whose y values range from -1.5
to 1.5.

The Mandelbrot set is a set of complex numbers
z, where f(z) = z**2+c that is bounded when
we continuously apply f to itself multiple times. 

INITIAL PARAMETERS: 
  
  No user inputs required. 
  
  N_times = 50: The number of times we iterate f
  some_threshold = 50: The number that we bound 
  our sequence by 
  
  points = 1000. More points makes the resulting 
  figure more detailed to match the picture in the
  assigment.    

OUTPUTS: 

  A png file entitled "mandelbrot.png"
  displaying our result
  
Other python files required: None
Subfunctions: None
            
AUTHOR: Roberto Bertolini
ID: 111635119
Email: roberto.bertolini@stonybrook.edu
Date: October 28, 2017

'''

import numpy as np

# Initial parameters (described above)

points = 1000
N_max = 50
some_threshold = 50

'''
The linspace command creates a 
sequence of equally spaced points
from a range of values. For example,
x is an ndarray of length 1000 that
contains 1000 equally spaced points
between -2 and 1. 
'''

x = np.linspace(-2, 1, points)
y = np.linspace(-1.5, 1.5, points)

'''
The newaxis command increases the
dimension of the existing array.
In this case, we create a new array
which takes each entry and puts it 
below the existing entry. The arrays
are the same length but are structured 
differently.

For example, 
array = [1,2,3]
n_array = array[:,np.newaxis]
n_array = [[1]
           [2]
           [3]]

This will help create our mask.

x contains the real part and y
contains the imaginary part
'''

x_new = x[:,np.newaxis]
y_new = y[np.newaxis,:]

# Combines the real and complex 
# part together

c = x_new +1j*y_new

# Mandelbrot iteration

z = 0
for j in range(N_max):
    z = z**2 + c
    
    # Append z to mask if it falls below
    # the threshold of 50
    
    mask = (abs(z) < some_threshold)

# Saves the resulting image of the
# Mandelbrot fractal to a png
# file entitled 'mandelbrot.png'.

import matplotlib.pyplot as plt


#The .T command takes the tranpose of my mask
#grid to plot the graph appropraitely and 
#specifies the range of x and y values in the 
#array.

plt.imshow(mask.T,extent = [-2,1,-1.5,1.5])

# Grayscale plot 

plt.gray()
#plt.savefig('mandelbrot.png')

