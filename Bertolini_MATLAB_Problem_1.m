%BERTOLINI_MATLAB_Problem_1.m
%   This program uses the Monte Carlo approach to approximate "pi/4" 
%   using the relative areas of a square and an inscribed circle. This 
%   program compares the estimate using a for and while loop to approximate 
%   the value of pi/4. For the for loop, the user inputs the number of data
%   points and the program calculates the ratio of random points that lie
%   in the circle and outside the circle. On the other hand, the while loop
%   prints an estimate of pi using an iterative method rounded off to a 
%   specified number of significant figures.
%
% INPUTS: 
%       n: number of randomly generated points for the for loop estimation 
%       sig: number of significant figures for the while loop estimation
%
% OUTPUTS: 
%       ratio: estimated value of pi using the for loop
%       result: estimated value of pi using the while loop
%       
% Other m files required: None
% Subfunctions: None
            
%AUTHOR: Roberto Bertolini
%ID: 111635119
%Email: roberto.bertolini@stonybrook.edu
%Date: October 20, 2017

%%%%%%%%%%%%%%%%%%%%%%%%%%%BEGIN MAIN FUNCTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Bertolini_MATLAB_Problem_1()

%%% BEGIN FOR LOOP ALGORITHM

n=input('Number of points (for loop): '); % use input
x=rand(n,1); % random x coordinate
y=rand(n,1); % random y coordinate
r=x.^2+y.^2; % square each coordinate

inside=0;   %Number of points inside circle

for i=1:n
    if r(i)<=0.25 % if r<.25, the point lies inside the circle
        inside=inside+1; % add one to the inside count
    end
end

% Calculate and print the ratio
ratio = inside/(.25*n); 
fprintf("Ratio of pi/4 using the for loop %s \n", ratio)

% Note that as the number of points increases, our answer converges to
% pi/4.

%%% END FOR LOOP ALGORITHM

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% BEGIN WHILE LOOP ALGORITHM

m = 0; % total number of points
j = 0; % point is inside the circle
radius = 1; % radius of the circle

sig=input('Number of significant figures (while loop): '); % user input

pi_init = 1000; % initial value of pi
precision = .1^(sig); % level of precision based on significant figures

while (true)
    x1 = rand(); % gives random float between 0 and 1 
    y1 = rand(); % gives random float between 0 and 1
    m = m+1;
    
    ratio_pi = j / (.25*m);
    pi_diff = abs(pi_init - ratio_pi); % difference between initial pi and ratio
    pi_init = ratio_pi;

    if (pi_diff <= precision)
        break; % once we reach our level of precision, we break out of the while loop
    end

    if (x1^2+y1^2<=radius^2)
        j = j+1; % if we do not break out of the loop, add one to the numerator tally
    end
end

ratio_while = j/m;

result= num2str(ratio_while,sig);
result= str2num(result);

%result = round(ratio_while,sig,'significant');
fprintf("Ratio of pi/4 using the while loop %f \n", result)
fprintf("Number of Iterations: %f \n", m)

end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%END MAIN FUNCTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%
